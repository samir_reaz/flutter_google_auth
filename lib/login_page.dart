import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _isLogIn = false;
  GoogleSignIn _googleSignIn = GoogleSignIn(scopes: ['email']);
  FirebaseAuth _auth = FirebaseAuth.instance;

  /*Future<FirebaseUser> _handleSignIn() async {
    GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    GoogleSignInAuthentication googleAuth = await googleUser.authentication;

    AuthCredential credential = GoogleAuthProvider.getCredential(
      idToken: googleAuth.idToken,
      accessToken: googleAuth.accessToken,
    );

    FirebaseUser user = (await _auth.signInWithCredential(credential)).user;
    print('signd in ' + user.displayName);
    return user;
  }*/

  _login() async {
    try {
      //await _googleSignIn.signIn();
      GoogleSignInAccount googleUser = await _googleSignIn.signIn();
      GoogleSignInAuthentication googleAuth = await googleUser.authentication;
      AuthCredential credential = GoogleAuthProvider.getCredential(
        idToken: googleAuth.idToken,
        accessToken: googleAuth.accessToken,
      );
      FirebaseUser user = (await _auth.signInWithCredential(credential)).user;
      setState(() {
        //_handleSignIn();
        _isLogIn = true;
      });
    } catch (e) {
      print(e);
    }
  }

  _logout() {
    _googleSignIn.signOut();
    setState(() {
      _isLogIn = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _isLogIn
            ? Column(
                children: <Widget>[
                  Image.network(_googleSignIn.currentUser.photoUrl,
                      height: 50, width: 50),
                  Text(_googleSignIn.currentUser.displayName),
                  Text(_googleSignIn.currentUser.email),
                  RaisedButton(
                    onPressed: () {
                      _logout();
                    },
                    child: Text('Sign Out'),
                  )
                ],
              )
            : RaisedButton(
                onPressed: () {
                  _login();
                },
                child: Text('Google Sign In'),
              ),
      ),
    );
  }
}
